const express = require('express');
const User = require('../models/User');
const nanoid = require('nanoid');

const createRouter = () => {
    const router = express.Router();

    router.post('/', (req, res) => {
        const newUser = req.body;
        newUser.token = nanoid(9);
        const user = new User(newUser);

        user.save()
            .then(user => res.send(user))
            .catch(error => res.status(400).send(error));
    });

    router.post('/sessions', async (req, res) => {
        const user = await User.findOne({username: req.body.username});

        if (!user) {
            return res.status(400).send({message: 'Username or password is not correct'});
        }

        const isMatch = await user.checkPassword(req.body.password);

        if (!isMatch) {
            return res.status(400).send({message: 'Username or password is not correct'});
        }
        user.token = nanoid(9);
        await user.save();
        res.send(user);
    });

    router.delete('/sessions', async (req, res) => {
        const token = req.get('token');
        const success = {message: 'Logout success!'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.token = nanoid(9);
        await user.save();

        return res.send(success);
    });

    return router;
};

module.exports = createRouter;
