const express = require('express');
const nanoid = require('nanoid');
const path = require('path');
const multer = require('multer');
const Album = require('../models/Album');
const config = require('../config');


const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {

    router.post('/', upload.single('image'), (req, res) => {
        const album = req.body;

        if (req.file) {
            album.image = req.file.filename;
        } else {
            album.image = "album.img";
        }
        const albums = new Album(album);
        albums.save()
            .then(result => res.send(result))
            .catch(() => res.send('Albom dont added'));
    });

    router.get('/:idAlbum', (req, res) => {
        if (req.params.idAlbum) {
            Album.find({author: req.params.idAlbum}, (error, result) => {
                if (error) res.status(404).send(error);
                if (result) res.send(result);
            }).populate('author')
        }
        else {
            Album.find().populate('author')
                .then(result => res.send(result))
                .catch((error) => res.send(error))
        }
    });

    router.get('/:id', (req, res) => {
            Album.findOne({_id: req.params.id}, (error, result) => {
                if (error) res.status(404).send(error);
                if (result) res.send(result);
            }).populate('author')
    });
    return router;
};

module.exports = createRouter;