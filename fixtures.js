const mongoose = require('mongoose');
const config = require('./config');

const Artist = require('./models/Artist');
const Track = require('./models/Track');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
    try {
        await db.dropCollection('artists');
    } catch (e) {
        console.log('Collections were not present, skipping drop...');
    }

    const [artist1, artist2, artist3, artist4, artist5, artist6] = await Artist.create({
        name: 'Король и Шут',
        image: 'some image'
    }, {
        title: 'HDDs',
        description: 'Hard Disk Drives'
    });

    await Product.create({
        title: 'Intel Core i7',
        price: 300,
        description: 'Very cool processor',
        category: cpuCategory._id,
        image: 'cpu.jpg'
    }, {
        title: 'Seagate 3TB',
        price: 110,
        desciption: 'Some kinda description',
        category: hddCategory._id,
        image: 'hdd.jpg'
    });

    db.close();
});